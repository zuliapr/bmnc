from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Berita
# Create your views here.
response ={}
def home(request):
    try:
        html = 'home.html'
        berita = Berita.objects.raw('SELECT * FROM BERITA')

        response['berita'] = berita
        return render(request, html, response)
            
    except Exception as e:
        print(e)
        return HttpResponseRedirect(reverse('home:index'))

def newArticle(request):
    try:
        if (request.session['status']=='true'):
            html = 'form.html'
            return render(request, html)
        else:
            print(e)
            return HttpResponseRedirect(reverse('home:index'))
    except Exception as e:
        print(e)
        return HttpResponseRedirect(reverse('home:index'))


def createNewArticle(request):
    if request.method == 'POST':
        print(request.POST['title'])
        print(request.POST['url'])
        print(request.POST['topic'])
        print(request.POST['content'])
        print(request.POST['tag'])

def createNewPoll(request):
    if request.method == 'POST':
        print(request.POST['desc'])
        print(request.POST['starting_date'])
        print(request.POST['end_date'])
        print(request.POST['hellomoto'])

def create_new_berita(url, judul, topik, tags, content, id_universitas):
    tags = tags.split(",")
    time_now = str(datetime.now())
    jumlah_kata = len(content.split(" "))
    
    latest_riwayat = Riwayat.objects.raw("SELECT * FROM RIWAYAT ORDER BY ID DESC LIMIT 1")
    id_riwayat = 1 if count(1 for i in latest_riwayat) else latest_riwayat[0].id + 1


    insert_berita = "INSERT INTO BERITA VALUES (%s, %s, %s, %s::timestamp, %s::timestamp, %s, %s, %s)"
    insert_riwayat = "INSERT INTO RIWAYAT VALUES (%s, %s, %s, %s)"
    insert_tag = "INSERT INTO TAG VALUES (url_berita, tag)"


    with connection.cursor() as cursor:
        cursor.execute(insert_berita, [url, judul, topik, time_now, time_now, jumlah_kata, 0, id_universitas])
        cursor.execute(insert_riwayat, [url, id_riwayat, time_now, content])
        
        for tag in tags:
            cursor.execute(insert_tag, [url, tag])